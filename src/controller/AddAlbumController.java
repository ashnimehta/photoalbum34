package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This controller handles adding a new album. It checks the user's 
* inputted text and confirms it is nonempty and nonnull, then 
* closes the dialog stage.
* 
* If the text inputted is invalid, it alerts the user that
* the album name must exist in order for the album 
* to be created. 
* 
* In case the user wishes to cancel (and not add the album), there's a method that
* handles the user pressing the cancel button and closes
* the dialog stage.
* 
*/

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class AddAlbumController{
	private Stage dialogStage;
	private String input;
	
	@FXML
	private TextField textInput;
	
    /**
	 * Default constructor
	 */   
    public AddAlbumController(){
    }	
    
    @FXML
    public void initialize(){
    	Platform.runLater(new Runnable() {
            @Override
            public void run() {
            	textInput.requestFocus();
            }
        });
    }
	
    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	@FXML
	private void handleOk(){
		if(textInput.getText().equals("") || textInput.getText() == null){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid New Album");
		    alert.setHeaderText("Album name not valid.");
		    alert.setContentText("Album name cannot be blank.");
		    alert.showAndWait();
		}
		else{
			input = textInput.getText();
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleAddCancel() {
		textInput.setText("");
		dialogStage.close();
	}
	
	public String getInput(){
		return input;
	}	
}