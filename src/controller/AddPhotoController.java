package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This controller handles the addition of photos to an album. Once an album exists, 
* a photo can be added to it. Within this controller, a FileChooser bar is loaded to 
* allow the user to select a photo file from their local machine.
* 
* Once a file is selected, the file path is set in the bar. 
* 
* If a user wishes to cancel before adding the file to their application,
* there is a method that handles a user pressing the cancel button.
*/

import java.io.File;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class AddPhotoController{
	private Stage dialogStage;
	private String path;
	
	@FXML
	private TextField textInput;
	
	public AddPhotoController(){	
	}
	
    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
    
	@FXML
	private void handleBrowse(){
		FileChooser fileChooser = new FileChooser();
		
		FileChooser.ExtensionFilter imageFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
		fileChooser.getExtensionFilters().add(imageFilter);

		File selectedFile = fileChooser.showOpenDialog(new Stage());
		if (selectedFile != null){
			textInput.setText(selectedFile.getAbsolutePath());
		}else textInput.setText("");
	}
	
	@FXML
	private void handleOk(){
		if(textInput.getText().equals("") || textInput.getText() == null){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid New Photo");
		    alert.setHeaderText("Pathname not valid.");
		    alert.setContentText("Pathname cannot be blank.");
		    alert.showAndWait();
		    
		    return;
		}
		File f = new File(textInput.getText());
		if(!(f.exists())) { 
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid New Photo");
		    alert.setHeaderText("Pathname not valid.");
		    alert.setContentText("Path must point to a photo on your machine.");
		    alert.showAndWait();
		    
		    return;
		}
		else{
			path = textInput.getText();
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleCancel() {
		textInput.setText("");
		dialogStage.close();
	}
	
	public String getPath(){
		return path;
	}
	
}