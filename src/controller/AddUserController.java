package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This class is a controller that is used exclusively by the admin user.
* It handles adding a new user to the application.
* 
* It ensures that the new user's login is nonnull, nonempty, and has no spaces.
* It then closes the dialog stage and routes back to the view that shows
* the admin the list of users.
* 
*/

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class AddUserController{
	private Stage dialogStage;
	private String input;
	
	@FXML
	private TextField textInput;
	
    /**
	 * Default constructor
	 */   
    public AddUserController(){
    }	
    
    @FXML
    public void initialize(){
    	Platform.runLater(new Runnable() {
            @Override
            public void run() {
            	textInput.requestFocus();
            }
        });
    }
	
    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	@FXML
	private void handleOk(){
		if(textInput.getText().equals("") || textInput.getText() == null){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid Username");
		    alert.setHeaderText("Username not valid.");
		    alert.setContentText("Username cannot be blank.");
		    alert.showAndWait();
		}
		if(textInput.getText().contains(" ")){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid Username");
		    alert.setHeaderText("Username not valid.");
		    alert.setContentText("Username cannot include spaces.");
		    alert.showAndWait();
		}
		if(textInput.getText().equals("admin")){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid Username");
		    alert.setHeaderText("Username not valid.");
		    alert.setContentText("Username cannot be \"admin\".");
		    alert.showAndWait();
		}
		else{
			input = textInput.getText();
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleAddCancel() {
		textInput.setText("");
		dialogStage.close();
	}
	
	public String getInput(){
		return input;
	}

}