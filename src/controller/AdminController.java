package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This class is the controller for admin users. It has a reference to the
* current instance of photo album, and a list of Strings that holds all
* user logins.
* 
* It sets up the view of users in an observable list, and handles addition
* and deletion of users. Within addition, the appropriate dialog box 
* is shown to allow admin to input a user they wish to add to the
* list of users. This user is then created and serialized, with their 
* (empty) information stored in <user>.txt. 
* 
* In the case of deletion, a dialog box is shown that confirms the admin
* wishes to delete the selected user. Once the admin presses OK within that dialog
* box, the User is deleted from the list of Strings that represents user logins, and
* the serialized file associated with that user is also deleted.
* 
*/

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import model.User;

public class AdminController{

    private PhotoAlbum photoAlbum;
	private ArrayList<String> users_str;
	
	private ObservableList<String> users_obs = FXCollections.observableArrayList();

	int selectedIndex = -1;
	
    @FXML
    private ListView<String> userList = new ListView<String>();
	
    /**
	 * Default constructor
	 */   
    public AdminController(){
    }
    
    /**
     * Initialize the controller
     */
    @FXML
    private void initialize(){
    }
    
    public void setup(){
    	users_obs.addAll(users_str);    	
    	userList.setItems(users_obs);
    	userList.getSelectionModel().select(0);
    }
    
	/**
	 * Save state and log out
	 */	
	@FXML
    private void handleLogout(){
		photoAlbum.getPrimaryStage().close();
		photoAlbum.loadLogin();
		return;
	}
    
	/**
	 * Delete selected user
	 */
	@FXML
	private void handleDeleteUser(){
		selectedIndex = userList.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			boolean okClicked = this.showDeleteDialog();
			if (okClicked){
				String username = userList.getSelectionModel().getSelectedItem().toString();
				userList.getItems().remove(selectedIndex);
				if(userList.getItems().size() <= 0){
					selectedIndex = -1;
				}
				for(int i = 0; i<users_str.size(); i++){
					if(users_str.get(i).equals(username)){
						users_str.remove(i);
						break;
					}
				}
				File file = new File("../PhotoAlbum34/data/" + username + ".ser");
				file.delete();
				photoAlbum.writeUsers(users_str);
				userList.getSelectionModel().selectNext();
			}
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Delete Error");
			alert.setHeaderText("No user selected.");
			alert.setContentText("Please select a user to delete.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Pull up delete confirmation
	 */
	private boolean showDeleteDialog(){
	    try {
	        // Load FXML file
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(PhotoAlbum.class.getResource("/view/DeleteDialogUser.fxml"));
	        AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Delete User");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        Scene scene = new Scene(pane);
	        dialogStage.setScene(scene);

	        //Set controller and current user into dialog
	        DeleteDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);

	        // Show dialog until user close
	        dialogStage.showAndWait();
	        
	        return controller.isOkClicked();

	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
	}
	
	/*
	 * Add user
	 */
	@FXML
	private void handleAddUser(){
	String newUsername = this.showAddUser();
		//if username is empty or null then user hit cancel 
		if (newUsername == "" || newUsername == null){
			return;
		}
		//make sure user does not already exist
		for (int i=0; i<users_str.size(); i++){
			if (users_str.get(i).equals(newUsername)){
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Add User Error");
				alert.setHeaderText("There is already a user by that name.");
				alert.setContentText("Duplicate users are not allowed.");
				alert.showAndWait();
				
				return;
			}
		}

		File file = new File("../PhotoAlbum34/data/" + newUsername + ".ser");
		try {
			file.createNewFile();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		users_obs.add(newUsername);
		photoAlbum.users_str.add(newUsername);
		User temp = new User(newUsername);
		
		try {
	         FileOutputStream fileOut = new FileOutputStream("../PhotoAlbum34/data/" + newUsername + ".ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(temp);
	         out.close();
	         fileOut.close();
	      }catch(IOException i) {
	         i.printStackTrace();
	      }
		
		photoAlbum.writeUsers(users_str);
		userList.getSelectionModel().select(newUsername);	
	}

	/*
	 * Show "add user" dialog
	 */
	private String showAddUser(){
	    try {
	        // Load FXML file
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(PhotoAlbum.class.getResource("/view/AddUser.fxml"));
	        AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Add User");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        Scene scene = new Scene(pane);
	        dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
	        AddUserController controller = loader.getController();
	        controller.setDialogStage(dialogStage);

	        // Show dialog until user close
	        dialogStage.showAndWait();
	        
	        return controller.getInput();

	    } catch (IOException e) {
	        e.printStackTrace();
	        return "";
	    }
	}
	
	public void setPhotoAlbum(PhotoAlbum photoAlbum){
		this.photoAlbum = photoAlbum;
	}

	public void setUsers_str(ArrayList<String> users_str){
		this.users_str = users_str;
	}

}