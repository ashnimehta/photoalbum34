package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This controller handles the view of albums. It displays the relevant data 
* in each album, like the date range of photos contained in that album, the name
* of that album, and the number of photos in that album.
* 
* If the user chooses to log out from this stage, the user (with changes made) is serialized so that
* state is maintained.
* 
* This controller also handles addition, deletion, and renaming of albums.
* 
* There are also methods that handle opening an album to view its contents and a method to handle search.
* 
*/

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;

public class AlbumListController{
    private PhotoAlbum photoAlbum;
    private User user;
    private ArrayList<Album> albums = new ArrayList<Album>();	
	private ObservableList<Album> albums_obs = FXCollections.observableArrayList();
	int selectedIndex = -1;
	private Tag tag1;
	private Tag tag2;
	private Tag tag3;
	private Calendar start;
	private Calendar end;
	
    @FXML
    private ListView<Album> albumList = new ListView<Album>();
	@FXML
	private Label nameLabel;
	@FXML
	private Label countLabel;
	@FXML
	private Label startDateLabel;
	@FXML
	private Label endDateLabel;
	
    /**
	 * Default constructor
	 */   
    public AlbumListController(){
    }
    
    /**
     * Populate ObservableList and ListView from ArrayList of albums
     */
    public void setup(){
    	albums = user.getAlbums();
    	albums_obs.addAll(albums);
    	albumList.setItems(albums_obs);
    	
	    // set listener for list
    	albumList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showDetails(newValue));
    	albumList.getSelectionModel().select(0);
    }
    
	void showDetails(Album album){
		if (album != null) {
			selectedIndex = albumList.getSelectionModel().getSelectedIndex();
						
			nameLabel.setText(album.getName());
			countLabel.setText(Integer.toString(album.getPhotos().size()));
			startDateLabel.setText(album.getStartDate());
			endDateLabel.setText(album.getEndDate());

		} else {
			nameLabel.setText("");
			countLabel.setText("");
			startDateLabel.setText("");
			endDateLabel.setText("");
		}
	}
	
	/**
	 * Save state and log out
	 */	
	@FXML
    public void handleLogout(){
		
		File file = new File("../PhotoAlbum34/data/" + user.getUsername() + ".ser");
		try {
			file.createNewFile();
		}catch (IOException e) {
			e.printStackTrace();
		}
		try {
	         FileOutputStream fileOut = new FileOutputStream("../PhotoAlbum34/data/" + user.getUsername() + ".ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(user);
	         out.close();
	         fileOut.close();
	    }catch(IOException i) {
	         i.printStackTrace();
	    }
		
		photoAlbum.getPrimaryStage().close();
		photoAlbum.loadLogin();
		return;
	}
	
	/**
	 * Delete selected album
	 */
	@FXML
	private void handleDeleteAlbum(){
		selectedIndex = albumList.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			boolean okClicked = this.showDeleteDialog();
			if (okClicked){
				String albumName = albumList.getSelectionModel().getSelectedItem().toString();

				//remove from ObsList
				albumList.getItems().remove(selectedIndex);
				if(albumList.getItems().size() <= 0){
					selectedIndex = -1;
				}
				//remove from ArrayList<Album>
				for(int i = 0; i<albums.size(); i++){
					if(albums.get(i).getName().equals(albumName)){
						albums.remove(i);
						break;
					}
				}
				albumList.getSelectionModel().selectNext();
			}
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Delete Error");
			alert.setHeaderText("No album selected.");
			alert.setContentText("Please select a album to delete.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Pull up delete confirmation
	 */
	private boolean showDeleteDialog(){
	    try {
	        // Load FXML file
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(PhotoAlbum.class.getResource("/view/DeleteDialogAlbum.fxml"));
	        AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Delete Album");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        Scene scene = new Scene(pane);
	        dialogStage.setScene(scene);

	        //Set controller and current user into dialog
	        DeleteDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);

	        // Show dialog until user close
	        dialogStage.showAndWait();
	        
	        return controller.isOkClicked();

	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
	}
	
	/**
	 * Add album
	 */
	@FXML
	private void handleAddAlbum(){
	String newAlbumName = this.showAddAlbum();
		//if album name is empty or null then user hit cancel 
		if (newAlbumName == "" || newAlbumName == null){
			return;
		}
		//make sure album does not already exist
		for (int i=0; i<albums.size(); i++){
			if (albums.get(i).getName().equals(newAlbumName)){
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Add Album Error");
				alert.setHeaderText("There is already a album by that name.");
				alert.setContentText("Duplicate albums are not allowed.");
				alert.showAndWait();
				
				return;
			}
		}
		Album newAlbum = new Album(user.getUsername(), newAlbumName);
		albums.add(newAlbum);
		albumList.getItems().add(newAlbum);
				
		albumList.getSelectionModel().select(newAlbum);
	}

	/**
	 * Show "add album" dialog
	 */
	private String showAddAlbum(){
	    try {
	        // Load FXML file
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(PhotoAlbum.class.getResource("/view/AddAlbum.fxml"));
	        AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Add Album");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        Scene scene = new Scene(pane);
	        dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
	        AddAlbumController controller = loader.getController();
	        controller.setDialogStage(dialogStage);

	        // Show dialog until user close
	        dialogStage.showAndWait();
	        
	        return controller.getInput();

	    } catch (IOException e) {
	        e.printStackTrace();
	        return "";
	    }
	}
	
	/**
	 * Rename selected album
	 */
	@FXML
	private void handleRename(){
		selectedIndex = albumList.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			String oldAlbumName = albumList.getSelectionModel().getSelectedItem().toString();
			String newAlbumName = this.showRenameAlbum();
			//if album name is empty or null then user hit cancel 
			if (newAlbumName == "" || newAlbumName == null){
				return;
			}
			//if album name is the same name
			if (newAlbumName.equals(oldAlbumName)){
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Rename Error");
				alert.setHeaderText("Album cannot be renamed to itself.");
				alert.setContentText("This is already the name of the album.");
				alert.showAndWait();
				
				return;
			}
			//check if album exists
			for (int i=0; i<albums.size(); i++){
				if (albums.get(i).getName().equals(newAlbumName)){
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Rename Error");
					alert.setHeaderText("There is already a album by that name.");
					alert.setContentText("Duplicate albums are not allowed.");
					alert.showAndWait();
					
					return;
				}
			}
			for (int i=0; i<albums.size(); i++){
				if (albums.get(i).getName().equals(oldAlbumName)){
					albums.get(i).setName(newAlbumName);
					
					albumList.getItems().remove(selectedIndex);
					albumList.getItems().add(albums.get(i));
					
					albumList.getSelectionModel().select(albums.get(i));
					
					break;
				}
			}
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Rename Error");
			alert.setHeaderText("No album selected.");
			alert.setContentText("Please select a album to rename.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Pull up dialog to rename album
	 */
	private String showRenameAlbum(){
	    try {
	        // Load FXML file
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(PhotoAlbum.class.getResource("/view/RenameAlbum.fxml"));
	        AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Rename Album");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        Scene scene = new Scene(pane);
	        dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
	        RenameAlbumController controller = loader.getController();
	        controller.setDialogStage(dialogStage);

	        // Show dialog until user close
	        dialogStage.showAndWait();
	        
	        return controller.getInput();

	    } catch (IOException e) {
	        e.printStackTrace();
	        return "";
	    }
	}
	
	@FXML
	private void handleOpen(){
		selectedIndex = albumList.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			showAlbumView(albumList.getSelectionModel().getSelectedItem());
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Open Error");
			alert.setHeaderText("No album selected.");
			alert.setContentText("Please select a album to open.");
			alert.showAndWait();
		}
	}
	
	private void showAlbumView(Album album){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/AlbumView.fxml"));
			VBox pane = (VBox) loader.load();

			AlbumViewController controller = loader.getController();
			controller.setPhotoAlbum(this.photoAlbum);
			controller.setUser(user);
			controller.setAlbum(album);
			controller.setup();

			Scene scene = new Scene(pane);
			photoAlbum.getPrimaryStage().setScene(scene);
			photoAlbum.getPrimaryStage().centerOnScreen();
			photoAlbum.getPrimaryStage().show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void handleSearch(){
		String style = this.showSearchDialog();
		if (style.equals("tag")){
			ArrayList<Photo> list = new ArrayList<Photo>();
			
			if (tag1 != null){
				if (tag1.getType() != null && !tag1.getType().equals("")){
					list.addAll(user.searchByTagPair(tag1.getType(), tag1.getValue()));
				}else list.addAll(user.searchByTags(tag1.getValue()));
			}
			if (tag2 != null){
				if (tag2.getType() != null && !tag2.getType().equals("")){
					list.addAll(user.searchByTagPair(tag2.getType(), tag2.getValue()));
				}else list.addAll(user.searchByTags(tag2.getValue()));
			}
			if (tag3 != null){
				if (tag3.getType() != null && !tag3.getType().equals("")){
					list.addAll(user.searchByTagPair(tag3.getType(), tag3.getValue()));
				}else list.addAll(user.searchByTags(tag3.getValue()));
			}
			
			if (list.size() == 0) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Search Error");
				alert.setHeaderText("No photos found.");
				alert.setContentText("There are no photos in your library matching the search terms.");
				alert.showAndWait();
				return;
			}
			
			this.showSearchView(list);
			return;	
		}
		else if (style.equals("date")){
			ArrayList<Photo> list = new ArrayList<Photo>();
			
			list.addAll(user.searchByDate(start, end));
			
			if (list.size() == 0) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Search Error");
				alert.setHeaderText("No photos found.");
				alert.setContentText("There are no photos in your library matching the date range.");
				alert.showAndWait();
				return;
			}
			this.showSearchView(list);
			return;	
		}
		else return;
	}
	
	private String showSearchDialog(){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/SearchDialog.fxml"));
	        VBox pane = (VBox) loader.load();

	        // Create new Stage for dialog
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Search");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        Scene scene = new Scene(pane);
	        dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
	        SearchDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);

	        // Show dialog until user close
	        dialogStage.showAndWait();
	        
	        tag1 = controller.getTag1();
	        tag2 = controller.getTag2();
	        tag3 = controller.getTag3();
	        start = controller.getStartDate();
	        end = controller.getEndDate();
	        
	        return controller.getSearchStyle();

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private void showSearchView(ArrayList<Photo> list){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/SearchView.fxml"));
			VBox pane = (VBox) loader.load();

			SearchViewController controller = loader.getController();
			controller.setPhotoAlbum(this.photoAlbum);
			controller.setUser(user);
			controller.setList(list);
			controller.setup();

			Scene scene = new Scene(pane);
			photoAlbum.getPrimaryStage().setScene(scene);
			photoAlbum.getPrimaryStage().centerOnScreen();
			photoAlbum.getPrimaryStage().show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setPhotoAlbum(PhotoAlbum photoAlbum){
		this.photoAlbum = photoAlbum;
	}
	public void setUser(User user){
		this.user = user;
	}
}