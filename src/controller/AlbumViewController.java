package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This controller handles the view of albums in more detail.
* The user can add a new photo from their file system, delete an existing photo,
* move and copy a photo to an existing album, and search.
* 
* If the user wishes to return back to the previous stage, it closes
* the current stage and shows the album view again for the current user.
* 
* It also shows previews of photos, with their captions.
* 
* 
*/


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import model.Album;
import model.Photo;
import model.Tag;
import model.User;

public class AlbumViewController{
	private PhotoAlbum photoAlbum;
	private User user;
	private Album album;
	private ObservableList<Photo> photos_obs = FXCollections.observableArrayList();
	private ObservableList<Tag> tags_obs = FXCollections.observableArrayList();
	private int selectedIndex = -1;
	
	@FXML private TableView<Photo> table;
	@FXML private TableColumn<Photo, Image> photoCol;
	@FXML private TableColumn<Photo, String> captionCol;
	
	@FXML private ListView<Tag> tagList;
	@FXML private Text captionLabel;
	@FXML private Label dateLabel;
	@FXML private ImageView mainDisplay;
	
	/**
     * Default constructor
     */	
	public AlbumViewController(){
	}

	public void setup(){
        //add photos to observable list
		photos_obs.addAll(album.getPhotos());
		table.setItems(photos_obs);

		photoCol.setCellValueFactory(new PropertyValueFactory<Photo, Image>("image"));
		captionCol.setCellValueFactory(new PropertyValueFactory<Photo, String>("caption"));
		
		//cell factory stuff to show thumbnails. doesn't work.
		photoCol.setCellFactory(new Callback<TableColumn<Photo,Image>, TableCell<Photo,Image>>(){
			@Override
			public TableCell<Photo,Image> call(TableColumn<Photo,Image> param){
				ImageView iv = new ImageView();
				TableCell<Photo,Image> cell = new TableCell<Photo,Image>(){
					@Override
					public void updateItem(Image item, boolean empty){
						iv.setFitWidth(400);
						iv.setPreserveRatio(true);
						iv.setImage(item);
					}
				};
				cell.setGraphic(iv);
				return cell;
			}
		});
		
		table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showDetails(newValue));
		table.getSelectionModel().select(0);
	}
	
	private void showDetails(Photo photo){
		if (photo != null){
			try {
				mainDisplay.setImage(new Image(new FileInputStream(photo.getLocation())));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			if (photo.getCaption() != null){
				captionLabel.setText(photo.getCaption());
			} else captionLabel.setText("");

			if (photo.getDateString() != null){
				dateLabel.setText(photo.getDateString());
			}else dateLabel.setText("");
			
			if (photo.getTags() != null && photo.getTags().size() != 0){
				tags_obs.setAll(table.getSelectionModel().getSelectedItem().getTags());
				tagList.setItems(tags_obs);
			}else tagList.setItems(null);
		}
		else{
			mainDisplay.setImage(null);
			captionLabel.setText("");
			tagList.setItems(null);
		}
	}
	
	@FXML
	private void handleReturn(){
		photoAlbum.getPrimaryStage().close();
		photoAlbum.showAlbumList(user);
		return;
	}
	
	@FXML
	private void handleAdd(){
		String path = showAddDialog();
		//if pathname is empty or null then user hit cancel 
		if (path == "" || path == null){
			return;
		}
		File temp = new File(path);
		//make sure pathname does not already exist
		for (int i=0; i<album.getPhotos().size(); i++){
			if (album.getPhotos().get(i).getLocation().equals(temp)){
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Add Photo Error");
				alert.setHeaderText("The specified photo is already in this album.");
				alert.setContentText("Duplicate photos are not allowed.");
				alert.showAndWait();
				
				return;
			}
		}

		File f = new File(path);
		this.album.addPhoto(f, this.album);
		photos_obs.add(this.album.getPhotos().get(this.album.getPhotos().size()-1));
		
		table.getSelectionModel().select(this.album.getPhotos().get(this.album.getPhotos().size()-1));
	}
	
	private String showAddDialog(){
		try {
	        // Load FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/AddPhoto.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Add Photo");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
			AddPhotoController controller = loader.getController();
			controller.setDialogStage(dialogStage);

	        // Show dialog until user close
			dialogStage.showAndWait();
			
			return controller.getPath();

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	@FXML
	private void handleDelete(){
		selectedIndex = table.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			boolean okClicked = this.showDeleteDialog();
			if (okClicked){
				album.deletePhoto(table.getSelectionModel().getSelectedItem());
				table.getItems().remove(selectedIndex);
				if(table.getItems().size() <= 0){
					selectedIndex = -1;
					showDetails(null);
				}
				else table.getSelectionModel().selectNext();
			}
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Delete Error");
			alert.setHeaderText("No photo selected.");
			alert.setContentText("Please select a photo to delete.");
			alert.showAndWait();
		}
	} 
	
	/**
	 * Pull up delete confirmation
	 */
	private boolean showDeleteDialog(){
		try {
	        // Load FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/DeleteDialogPhoto.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Delete Photo");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

	        //Set controller and current user into dialog
			DeleteDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);

	        // Show dialog until user close
			dialogStage.showAndWait();
			
			return controller.isOkClicked();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@FXML
	private void handleCaption(){
		selectedIndex = table.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			String caption = this.showCaptionDialog();
			//if caption is empty or null then user hit cancel 
			if (caption == "" || caption == null){
				return;
			}
			
			//File f = new File(table.getSelectionModel().getSelectedItem().getLocation().getAbsolutePath());
			//this.album.addPhoto(f, this.album);
			
			table.getSelectionModel().getSelectedItem().setCaption(caption);
			showDetails(table.getSelectionModel().getSelectedItem());
			table.refresh();
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Caption Error");
			alert.setHeaderText("No photo selected.");
			alert.setContentText("Please select a photo to edit caption.");
			alert.showAndWait();
		}
		
	}
	
	private String showCaptionDialog(){
		try {
	        // Load FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/CaptionDialog.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Change Caption");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
			CaptionController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setTextInput(table.getSelectionModel().getSelectedItem().getCaption());

	        // Show dialog until user close
			dialogStage.showAndWait();
			
			return controller.getInput();

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	@FXML
	private void handleTags(){
		selectedIndex = table.getSelectionModel().getSelectedIndex();
		boolean refresh = false;
		if(selectedIndex >= 0){
			refresh = showTagList(table.getSelectionModel().getSelectedItem());
			if (refresh) showDetails(table.getSelectionModel().getSelectedItem());
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Tag Error");
			alert.setHeaderText("No photo selected.");
			alert.setContentText("Please select a photo to edit tags.");
			alert.showAndWait();
		}
	}
	
	private boolean showTagList(Photo photo){
		try {
	        // Load FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/TagList.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Tag List");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
			TagListController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setPhoto(photo);
			controller.setup();

	        // Show dialog until user close
			dialogStage.showAndWait();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@FXML
	private void handleMove(){
		selectedIndex = table.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			String newAlbumName = this.showMoveDialog();
			if (newAlbumName == "" || newAlbumName == null){
				return;
			}
			
			for (int i = 0; i<user.getAlbums().size(); i++){
				if(user.getAlbums().get(i).getName().equals(newAlbumName)){
					for (int j=0; j<user.getAlbums().get(i).getPhotos().size(); j++){
						if (user.getAlbums().get(i).getPhotos().get(j).getLocation().equals(table.getSelectionModel().getSelectedItem().getLocation())){
							Alert alert = new Alert(AlertType.WARNING);
							alert.setTitle("Move Error");
							alert.setHeaderText("Duplicate photo in specified album.");
							alert.setContentText("A photo with this pathname already exists in the specified album.");
							alert.showAndWait();
							return;
						}
					}
					table.getSelectionModel().getSelectedItem().moveToAlbum(user.getAlbums().get(i));
					photos_obs.remove(selectedIndex);
					return;
				}
			}
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Move Error");
			alert.setHeaderText("Album invalid.");
			alert.setContentText("The specified album does not exist.");
			alert.showAndWait();
			
			return;
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Move Error");
			alert.setHeaderText("No photo selected.");
			alert.setContentText("Please select a photo to move.");
			alert.showAndWait();
		}
	}
	
	private String showMoveDialog(){
		try {
	        // Load FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/MovePhoto.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Move Photo");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
			MovePhotoController controller = loader.getController();
			controller.setDialogStage(dialogStage);

	        // Show dialog until user close
			dialogStage.showAndWait();
			
			return controller.getInput();

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	@FXML
	private void handleCopy(){
		selectedIndex = table.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			String newAlbumName = this.showCopyDialog();
			
			if (newAlbumName == "" || newAlbumName == null){
				return;
			}

			for (int i = 0; i<user.getAlbums().size(); i++){
				if(user.getAlbums().get(i).getName().equals(newAlbumName)){
					table.getSelectionModel().getSelectedItem().copyToAlbum(user.getAlbums().get(i));
					return;
				}
			}
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Copy Error");
				alert.setHeaderText("Album invalid.");
				alert.setContentText("The specified album does not exist.");
				alert.showAndWait();
				
				return;
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Copy Error");
			alert.setHeaderText("No photo selected.");
			alert.setContentText("Please select a photo to copy.");
			alert.showAndWait();
		}
	}
	
	private String showCopyDialog(){
		try {
	        // Load FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/CopyPhoto.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

	        // Create new Stage for dialog
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Copy Photo");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

	        //Set controller and Stage into dialog
			CopyPhotoController controller = loader.getController();
			controller.setDialogStage(dialogStage);

	        // Show dialog until user close
			dialogStage.showAndWait();
			
			return controller.getInput();

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public void setPhotoAlbum(PhotoAlbum photoAlbum){
		this.photoAlbum = photoAlbum;
	}
	public void setUser(User user){
		this.user = user;
	}
	public void setAlbum(Album album){
		this.album = album;
	}
}