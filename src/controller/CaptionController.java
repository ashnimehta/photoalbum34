package controller;

/**
 * CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531).
* 
 * This controller handles the caption for each photo.
 * It ensures that the caption is nonempty and nonnull.
 */

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class CaptionController{
	private Stage dialogStage;
	private String input;
	
	@FXML
	private TextField textInput;
	
    /** 
	 * Default constructor
	 */   
    public CaptionController(){
    }	
	
    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
    
    public void setTextInput(String text){
    	textInput.setText(text);
    }

	@FXML
	private void handleOk(){
		if(textInput.getText().equals("") || textInput.getText() == null){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid Caption");
		    alert.setHeaderText("Caption not valid.");
		    alert.setContentText("Caption cannot be blank.");
		    alert.showAndWait();
		}
		else{
			input = textInput.getText();
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleCancel() {
		textInput.setText(null);
		dialogStage.close();
	}
	
	public String getInput(){
		return input;
	}	
	
	
	
}