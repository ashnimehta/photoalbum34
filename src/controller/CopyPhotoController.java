package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This controller handles copying a photo from one album to another.
* 
* It first checks the user's input, to make sure it is nonempty and nonnull.
* It then confirms that the user's inputted album title is one of their albums.
* If it is, it calls a method that copies the selected photo to the new album.
* 
* If it isn't, an alert is shown. This controller also gives the user the option to
* cancel and not copy the photo to another album. In this case, the dialog stage is 
* closed.
* 
*/

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class CopyPhotoController{
	
	private Stage dialogStage;
	private String input;
	
	@FXML
	private TextField textInput;
	
	public CopyPhotoController(){
	}
	
	@FXML
	private void handleOk(){
		if(textInput.getText().equals("") || textInput.getText() == null){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid New Album");
		    alert.setHeaderText("Album name not valid.");
		    alert.setContentText("Album name cannot be blank.");
		    alert.showAndWait();
		}
		else{
			input = textInput.getText();
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleCancel(){
		textInput.setText("");
		dialogStage.close();
	}
	
	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	public String getInput(){
		return input;
	}
	
	
}