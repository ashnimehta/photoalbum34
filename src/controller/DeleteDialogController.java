package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
*  This class holds the controller for our delete dialog boxes.
*  There are methods that handle confirmation that a user does
*  want to delete the selected item and that handle if a user wishes
*  to cancel their intended deletion of the item.
*  
*/

import javafx.fxml.FXML;
import javafx.stage.Stage;

public class DeleteDialogController{
    
    private Stage dialogStage;
    private boolean okClicked = false;
    
    /**
     * Default constructor
     */
    public DeleteDialogController() {
    }
    
    public void setDialogStage(Stage dialogStage){
    	this.dialogStage = dialogStage;
    }
    
    @FXML
    private void handleDeleteConfirm(){
    	okClicked = true;
    	dialogStage.close();
    }
    
    @FXML
    private void handleDeleteCancel(){
    	dialogStage.close();
    }
	
    /**
     * Getters and setters
     * 
     */
    public boolean isOkClicked(){
    	return okClicked;
    }

}