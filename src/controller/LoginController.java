package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
*  This class holds the controller for our login page.
*  It checks to make sure the text inputted by
*  a user is appropriately formatted (no spaces and
*  nonempty), and then sends the inputted text to another
*  method that will check whether the user
*  is a user within the application's list of users.
*  
*/

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class LoginController{
    @FXML
    private TextField usernameField;
    
	private Stage dialogStage;
    private String input;
   
    /**
	 * Default constructor
	 */   
    public LoginController(){
    }
    
    @FXML
    public void initialize(){
    	Platform.runLater(new Runnable() {
            @Override
            public void run() {
            	usernameField.requestFocus();
            }
        });
    }
    
	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
    @FXML
    private void handleOk(){
    	if(usernameField.getText().equals("")){
	    	Alert alert = new Alert(AlertType.ERROR);
		    alert.setTitle("Invalid Username");
		    alert.setHeaderText("Usernames cannot be blank.");
		    alert.setContentText("Please enter a username.");
		    alert.showAndWait();
    	}
    	else if(usernameField.getText().contains(" ")){
	    	Alert alert = new Alert(AlertType.ERROR);
		    alert.setTitle("Invalid Username");
		    alert.setHeaderText("Usernames cannot contains spaces.");
		    alert.setContentText("Please try a different username, or log in as \"admin\" and add your desired user.");
		    alert.showAndWait();
		}
    	else{			
    		input = usernameField.getText();
    		dialogStage.close();
    	}
    }
    
	public String getInput(){
		return input;
	}

}