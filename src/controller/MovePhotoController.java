package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
*  This class holds the controller for moving a photo
*  from one album to another.
*  
*  It first checks if the text inputted by the user is nonempty
*  and nonnull, then checks if the album title exists within 
*  the list of albums for the current user. If it does exist, it 
*  calls a method to move the photo to the album specified.
*  
*  If the album is not found in the list of albums for the current user,
*  a dialog box is shown with an error message, letting the current user 
*  know that the album name they inputted is not one of their albums.
*  
*  If the user wishes to cancel moving their photo, this controller
*  handles them pressing the cancel button, closes the current
*  dialog stage, and routes the user back to the previous screen.
*  
*/

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class MovePhotoController{
	
	private Stage dialogStage;
	private String input;
	
	@FXML
	private TextField textInput;
	
	public MovePhotoController(){
	}
	
	@FXML
	private void handleOk(){
		if(textInput.getText().equals("") || textInput.getText() == null){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid New Album");
		    alert.setHeaderText("Album name not valid.");
		    alert.setContentText("Album name cannot be blank.");
		    alert.showAndWait();
		}
		else{
			input = textInput.getText();
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleCancel(){
		textInput.setText("");
		dialogStage.close();
	}
	
	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	public String getInput(){
		return input;
	}
	
}