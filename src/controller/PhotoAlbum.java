package controller;

/**
* CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
* 
* This is the main class of our application. It holds our implementation
* of a Photo Album. It handles login of a user, whether that user is admin or not, 
* and reads and writes a text file of user logins to a file called
* "users.txt" located within the data folder. This file allows us to maintain
* a list of users that admin can access, without admin having access to a 
* particular user's data - albums, photos, tags.
* 
* There is also a current user of the application, specified by a String, that 
* allows us to keep track of who is currently using the application. This also
* allows us to deserialize the appropriate user data file (<user>.ser) when we
* load their list of albums in the application.
* 
*/

//import java libs
import java.util.ArrayList;
import java.io.*;

//import javafx libs
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;

//import proprietary packages
import model.User;

public class PhotoAlbum extends Application {

	private Stage primaryStage;
	public ArrayList<String> users_str;
	private User u = null;
	private String currentUser;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Photo Album");
		
		loadLogin();
		
		//if window is closed, saved current session
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we) {
				if (currentUser.equals("admin")) {
				} else {
					File file = new File("../PhotoAlbum34/data/" + currentUser + ".ser");
					try {
						file.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}

					try {
						FileOutputStream fileOut = new FileOutputStream("../PhotoAlbum34/data/" + currentUser + ".ser");
						ObjectOutputStream out = new ObjectOutputStream(fileOut);
						out.writeObject(u);
						out.close();
						fileOut.close();
					} catch (IOException i) {
						i.printStackTrace();
					}
				}
			}
		});
	}

	void loadLogin(){
		
		// Load in user data
		this.readUsers();
		currentUser = "";

		// Load login window
		while (currentUser == "") {
			currentUser = this.handleLogin();
		}

		// if user is admin, pull up admin page
		if (currentUser.equals("admin")) {
			this.showAdmin();
		}

		// create User object for user and pull up album list
		else {
			try {
				FileInputStream fileIn = new FileInputStream("../PhotoAlbum34/data/" + currentUser + ".ser");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				u = (User) in.readObject();
				in.close();
				fileIn.close();
			} catch (IOException i) {
				i.printStackTrace();
				return;
			} catch (ClassNotFoundException c) {
				c.printStackTrace();
				return;
			}
			this.showAlbumList(u);
		}
	}
	
	private String handleLogin() {
		String username = this.showLogin();

		// If username is null, then user closed the window, so close the
		// application
		if (username == null) {
			System.exit(0);
		}
		if (username.equalsIgnoreCase("admin")) {
			return "admin";
		}
		// TODO: loop through user list until user is found
		for (int i = 0; i < users_str.size(); i++) {
			if (users_str.get(i).equals(username)) {
				return username;
			}
		}
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("User Not Found");
		alert.setHeaderText("There is no user by that username.");
		alert.setContentText("Please try a different username, or log in as \"admin\" and add your desired user.");
		alert.showAndWait();

		return "";
	}

	private String showLogin() {
		try {
			// Load FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/Login.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

			// Create new Stage for dialog
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Login");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

			// Set controller and current user into dialog
			LoginController controller = loader.getController();
			controller.setDialogStage(dialogStage);

			// Show dialog until user close
			dialogStage.showAndWait();

			return controller.getInput();

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	private void showAdmin() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/Admin.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

			AdminController controller = loader.getController();
			controller.setPhotoAlbum(this);
			controller.setUsers_str(this.users_str);
			controller.setup();

			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void showAlbumList(User u) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PhotoAlbum.class.getResource("/view/AlbumList.fxml"));
			AnchorPane pane = (AnchorPane) loader.load();

			AlbumListController controller = loader.getController();
			controller.setPhotoAlbum(this);
			controller.setUser(u);
			controller.setup();

			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * File operations
	 */

	/**
	 * Read usernames from file and add to ArrayLists
	 */
	public void readUsers() {
		// Load usernames and user data from file
		File file = new File("../PhotoAlbum34/data/users.txt");
		String line = "";
		this.users_str = new ArrayList<String>();
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			while (!(line = br.readLine()).equals("-")) {
				this.users_str.add(line);
			}
			br.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}

	/**
	 * Save usernames from ArrayLists to file
	 */
	public void writeUsers(ArrayList<String> users_str) {
		File fileName = new File("../PhotoAlbum34/data/users.txt");
		try {
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			for (int i = 0; i < this.users_str.size(); i++) {
				bw.write(this.users_str.get(i));
				bw.newLine();
			}
			bw.write("-"); // signifies end of file
			bw.close();
			fw.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public Stage getPrimaryStage(){
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
