package controller;

/**
 * CS213 - Software Methodology - Fall 2016     
 * Assignment 3 - Photo Album              
 * @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
 * 
 * This class is the controller for renaming an album.
 * It checks to see if the new album name is nonnull and nonempty,
 * then sends the text input to a method that confirms it is 
 * not an album title that the User already holds.
 * If the user chooses to cancel and not rename the album, 
 * none of this is executed, and instead, the dialog stage is closed.
 *
 */

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RenameAlbumController{
	private Stage dialogStage;
	private String input;
	
	@FXML
	private TextField textInput;
	
    /**
	 * Default constructor
	 */   
    public RenameAlbumController(){
    }	
	
    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	@FXML
	private void handleOk(){
		if(textInput.getText().equals("") || textInput.getText() == null){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Invalid Album Rename");
		    alert.setHeaderText("Album name not valid.");
		    alert.setContentText("Album name cannot be blank.");
		    alert.showAndWait();
		}
		else{
			input = textInput.getText();
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleCancel() {
		textInput.setText("");
		dialogStage.close();
	}
	
	public String getInput(){
		return input;
	}	
	
}