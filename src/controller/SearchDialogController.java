package controller;

/**
 * CS213 - Software Methodology - Fall 2016     
* Assignment 3 - Photo Album              
* @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
 *
 * This is the controller that handles search. It allows
 * a user to search by date or by tag, and within search by tag, 
 * the user can search by a maximum of three tags. This controller ensures that
 * tag-types have been formatted correctly: that is, type-value pairs exist,
 * or if a user wishes to search just by value, that option is there as well.
 *
 */

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import model.Tag;

public class SearchDialogController{
	private Stage dialogStage;
	private Calendar startDate;
	private Calendar endDate;
	private Tag tag1;
	private Tag tag2;
	private Tag tag3;
	
	private String searchStyle = "";
	
	@FXML private DatePicker startDatePicker;
	@FXML private DatePicker endDatePicker;
	@FXML private TextField tagType1;
	@FXML private TextField tagVal1;
	@FXML private TextField tagType2;
	@FXML private TextField tagVal2;
	@FXML private TextField tagType3;
	@FXML private TextField tagVal3;
	
    /**
	 * Default constructor
	 */   
    public SearchDialogController(){
    }	
	
    public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	@FXML
	private void handleSearchByDate(){		
		startDate = Calendar.getInstance();
		endDate = Calendar.getInstance();
		
		LocalDate localStartDate = startDatePicker.getValue();
		LocalDate localEndDate = endDatePicker.getValue();
				
		Instant startInstant = localStartDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		startDate.setTime(Date.from(startInstant));
		
		Instant endInstant = localEndDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		endDate.setTime(Date.from(endInstant));
		endDate.add(Calendar.DATE, 1);
		
		searchStyle = "date";
		dialogStage.close();
	}
	
	@FXML
void handleSearchByTags(){
		boolean flag1 = false;
		boolean flag2 = false;
		boolean flag3 = false;
		
		//check if there's at least 1 tag value
		if((tagVal1.getText().equals("") || tagVal1.getText() == null) &&
				(tagVal2.getText().equals("") || tagVal2.getText() == null) &&
				(tagVal3.getText().equals("") || tagVal3.getText() == null)){
			flag1 = flag2 = flag3 = true;
		}
		else{	
			//tag pair 1
			if((tagVal1.getText().equals("") || tagVal1.getText() == null) &&
					!(tagType1.getText().equals("") || tagType1.getText() == null)){
				flag1 = true;
			}
			else if((tagVal1.getText().equals("") || tagVal1.getText() == null) &&
					(tagType1.getText().equals("") || tagType1.getText() == null)){
			}
			else tag1 = new Tag(tagType1.getText(),tagVal1.getText());
			
			//tag pair 2
			if((tagVal2.getText().equals("") || tagVal2.getText() == null) &&
					!(tagType2.getText().equals("") || tagType2.getText() == null)){
				flag2 = true;
			}
			else if((tagVal2.getText().equals("") || tagVal2.getText() == null) &&
					(tagType2.getText().equals("") || tagType2.getText() == null)){
			}
			else tag2 = new Tag(tagType2.getText(),tagVal2.getText());
			
			//tag pair 3
			if((tagVal3.getText().equals("") || tagVal3.getText() == null) &&
					!(tagType3.getText().equals("") || tagType3.getText() == null)){
				flag3 = true;
			}
			else if((tagVal3.getText().equals("") || tagVal3.getText() == null) &&
					(tagType3.getText().equals("") || tagType3.getText() == null)){
			}
			else tag3 = new Tag(tagType3.getText(),tagVal3.getText());
		}
		if (flag1 || flag2 || flag3){
			Alert alert = new Alert(AlertType.ERROR);
		    alert.initOwner(dialogStage);
		    alert.setTitle("Search Error");
		    alert.setHeaderText("Tags not valid.");
		    alert.setContentText("Cannot have tag type without value.");
		    alert.showAndWait();
		    
		    tag1 = tag2 = tag3 = null;
		    
		    searchStyle = "";
		    dialogStage.close();
		    return;
		}
		searchStyle = "tag";
		dialogStage.close();
	}
	
	@FXML
	private void handleCancel() {
		searchStyle = "";
		dialogStage.close();
	}
	
	public String getSearchStyle(){
		return this.searchStyle;
	}
	public Tag getTag1(){
		return this.tag1;
	}
	public Tag getTag2(){
		return this.tag2;
	}
	public Tag getTag3(){
		return this.tag3;
	}
	
	public Calendar getStartDate(){
		return this.startDate;
	}
	public Calendar getEndDate(){
		return this.endDate;
	}

}