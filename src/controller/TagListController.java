package controller;

/**
 * CS213 - Software Methodology - Fall 2016     
 * Assignment 3 - Photo Album              
 * @author Michelle Chen (mc1481) & Ashni Mehta (am1531)
 * 
 * This class is the controller for the list of tags.
 * It handles setting up the list on the front-end,
 * exiting the list and returning back to the previous screen,
 * adding a Tag and deleting a Tag.
 *
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import model.Photo;
import model.Tag;

public class TagListController{
	
	private Stage dialogStage;
	private ObservableList<Tag> tags_obs = FXCollections.observableArrayList();
	private Photo photo;
	private int selectedIndex = -1;
	
	@FXML private ListView<Tag> list;
	
	@FXML private TextField type;
	@FXML private TextField value;
	
	public TagListController(){
	}
	
	public void setup(){
		tags_obs.addAll(photo.getTags());
		list.setItems(tags_obs);
		list.getSelectionModel().select(0);
	}
	
	@FXML
    private boolean handleReturn(){
		dialogStage.close();
		return true;
    }
    
	@FXML
    private void handleAdd(){
		String t = type.getText();
		String v = value.getText();
		
		if (t == null || v == null || t.equals("") || v.equals("")){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Add Tag Error");
			alert.setHeaderText("Tag invalid.");
			alert.setContentText("Tag must be a type-value pair.");
			alert.showAndWait();
			
			return;
		}
		boolean flag = false;
		for(int i = 0; i < photo.getTags().size(); i++){
			if(photo.getTags().get(i).getType().equals(t)){
				if(photo.getTags().get(i).getValue().equals(v)){
					flag = true;
					//they are same type-value
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Add Tag Error");
					alert.setHeaderText("Tag already exists.");
					alert.setContentText("This tag already exists for this photo.");
					alert.showAndWait();
					break;
				}
			}
		}
		if(flag == true){
			return;
		}
		
		Tag temp = new Tag(t, v);
		photo.addTag(temp);
		tags_obs.add(temp);		
		type.setText("");
		value.setText("");
		
		list.getSelectionModel().select(temp);
    }
    
	@FXML
    private void handleDelete(){
		selectedIndex = list.getSelectionModel().getSelectedIndex();
		if(selectedIndex >= 0){
			photo.deleteTag(list.getSelectionModel().getSelectedItem());
			list.getItems().remove(selectedIndex);
			if(list.getItems().size() <= 0){
				selectedIndex = -1;
			}
			else list.getSelectionModel().selectNext();
		}
		else{
			// If no selection
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Delete Tag Error");
			alert.setHeaderText("No tag selected.");
			alert.setContentText("Please select a tag to delete.");
			alert.showAndWait();
		}
	} 
	
	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	public void setPhoto(Photo photo){
		this.photo = photo;
	}
	
	public String getInput(){
		return type + " " + value;
	}
}