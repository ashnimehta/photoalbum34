package model;

/**
 * 
 * @authors Michelle Chen and Ashni Mehta
 * 
 * This class holds our representation of Album.
 * Each album has an ArrayList of photos,
 * a start date that represents the beginning
 * of the date range of all photos contained,
 * an end date that holds the date of the most recently
 * modified file in the album, a username that maps
 * this album to the appropriate user, 
 * and a title, for the album name.
 * 
 * This class is serializable so that the User class can
 * serialize appropriately (as a User has Album fields within it).
 * 
 * In addition to getters and setters for the fields within this class,
 * Album.java also allows for photos to be added into albums. There are two
 * separate methods for this: one that adds new photos from the file system,
 * and another that handles photos that are already within another album, but
 * are either being moved or copied to this album.
 * 
 */

import java.util.ArrayList;
import java.io.*;
import java.util.Calendar;

public class Album implements Serializable{
	private String username;
	private String title;
	private ArrayList<Photo> photos;
	
	private String startDate;
	private String endDate;
	private Calendar startCal;
	private Calendar endCal;
	
	public Album(String username, String title){
		this.username = username;
		this.title = title;
		this.photos = new ArrayList<Photo>();
		
		this.startDate = "";
		this.endDate = "";
		this.startCal = Calendar.getInstance();
		this.endCal = Calendar.getInstance();
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setName(String title){
		this.title = title;
	}
	
	public String getName(){
		return this.title;
	}
	
	public ArrayList<Photo> getPhotos(){
		return this.photos;
	}
	
	public void setStartDate(String startDate){
		this.startDate = startDate;
	}
	
	public String getStartDate(){
		return this.startDate;
	}
	
	public void setEndDate(String endDate){
		this.endDate = endDate;
	}
	
	public String getEndDate(){
		return this.endDate;
	}
	
	public void addPhoto(File file, Album album){
		/*Handle adding photo?*/
		/*if valid file path, if date is earlier than startDate or later than endDate
		 * modify accordingly
		 */
		Photo temp = new Photo(file, album);
		this.getPhotos().add(temp);
		if(this.photos.size() == 1){
			//this photo is both start date and end date
			this.startCal = temp.getDateCal();
			this.endCal = temp.getDateCal();
			this.startDate = temp.getDateString();
			this.endDate = temp.getDateString();
		}
		else{
			if(temp.getDateCal().before(this.startCal)){
				this.startCal = temp.getDateCal();
				this.startDate = temp.getDateString();
			}
			if(temp.getDateCal().after(this.endCal)){
				this.endCal = temp.getDateCal();
				this.endDate = temp.getDateString();
			}
		}
		
		/*If photo is before start Date, start Date = photo Start date
		 * if photo is after end date, end date = photo end date
		 */
		return;
	}
	
	public void addExistingPhoto(Photo photo){
		Photo temp = new Photo(photo.getLocation(), this);
		temp.setTags(photo.getTags());
		temp.setCaption(photo.getCaption());
		
		this.getPhotos().add(temp);
	}
	
	public void deletePhoto(Photo photo){
		this.getPhotos().remove(photo);
		/*update start and end dates*/
		if(this.getPhotos().size() == 0){
			this.startCal = Calendar.getInstance();
			this.endCal = Calendar.getInstance();
			this.startDate = "";
			this.endDate = "";
			return;
		}
		else if(this.photos.size() == 1){
				//this photo is both start date and end date
				this.startCal = this.photos.get(0).getDateCal();
				this.endCal = this.photos.get(0).getDateCal();
				this.startDate = this.photos.get(0).getDateString();
				this.endDate = this.photos.get(0).getDateString();
				return;
		}
		else{
			this.startCal = this.photos.get(0).getDateCal();
			this.endCal = this.photos.get(0).getDateCal();
			this.startDate = this.photos.get(0).getDateString();
			this.endDate = this.photos.get(0).getDateString();
			//above: initialization of start and end date
			
			//below: loop through and update start and end as needed
			for(int i = 0; i<this.photos.size(); i++){
				if(this.photos.get(i).getDateCal().before(startCal)){
					this.startCal = this.photos.get(i).getDateCal();
					this.startDate = this.photos.get(i).getDateString();
				}
				if(this.photos.get(i).getDateCal().after(endCal)){
					this.endCal = this.photos.get(i).getDateCal();
					this.endDate = this.photos.get(i).getDateString();
				}
			}
			System.out.println(startDate);
			System.out.println(endDate);
		}
		return;
	}
	
	@Override
	public String toString(){
		return this.title;
	}
}