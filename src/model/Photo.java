package model;

/** 
 * @author Michelle Chen
 * @author Ashni Mehta
 * 
 * This class holds the data associated with each Photo (a caption, a list of tags,
 * the pathname to the photo file, and the Album in which
 * the photo resides. The class is serializable so that a User can be serialized.
 * This class, in addition to getters and setters for the fields, provides
 * functionality for copying and moving a photo to an existing album.
 */

import java.io.*;
import java.util.*;
import javafx.scene.image.Image;
import java.text.SimpleDateFormat;

public class Photo implements Serializable{
	private File location;
	private Album album;
	private String caption;
	private ArrayList<Tag> tags;
	
	private Calendar calendar;
	private String dateString;
	
	transient private Image image;
	
	public Photo(File location, Album album){
		this.location = location;
		this.album = album;
		this.caption = "";
		this.tags = new ArrayList<Tag>();
		
		calendar = Calendar.getInstance();
		this.calendar.setTimeInMillis(location.lastModified());
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		this.dateString = sdf.format(location.lastModified());
	}
	
	public Photo getPhoto(){
		return this;
	}

	public Image getImage(){
		try {
			this.image = new Image(new FileInputStream(location));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return this.image;
	}
	
	public Calendar getDateCal(){
		return this.calendar;
	}
	
	public String getDateString(){
		return this.dateString;
	}
	
	public File getLocation(){
		return this.location;
	}
	
	public void setCaption(String caption){
		this.caption = caption;
	}
	
	public String getCaption(){
		return this.caption;
	}
	public Album getAlbum(){
		return this.album;
	}
	
	public void setAlbum(Album album){
		this.album = album;
	}
	
	public ArrayList<Tag> getTags(){
		return this.tags;
	}
	
	public void setTags(ArrayList<Tag> tags){
		this.tags = tags;
	}
	
	public void addTag(Tag t){
		this.tags.add(t);
	}
	
	public void deleteTag(Tag t){
		this.tags.remove(t);
	}

	public void copyToAlbum(Album album){
		/* When this is called, we know that it's in the list of albums already
		 * Because the controller handles that..
		 * Create new Photo and add to new Album...
		*/
		Photo temp = new Photo(this.getLocation(), album);
		temp.setTags(this.getTags());
		temp.setCaption(this.caption);
		
		album.addExistingPhoto(this);
	}
	
	public void moveToAlbum(Album album){
		/*Move to new album then delete current copy*/
		/*Controller loops through to see if album name exists, so we know it exists.*/
		
		Photo temp = new Photo(this.getLocation(), album);
		temp.setTags(this.getTags());
		temp.setCaption(this.caption);
		
		album.addExistingPhoto(temp);
		
		this.album.deletePhoto(this);
	}
}