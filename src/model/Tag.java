package model;

/**
 * @author Ashni Mehta
 * @author Michelle Chen
 * 
 * This class holds our implementation of Tag. A tag has a type and a value
 * (for example, {Location, New York}), and we implemented both as Strings.
 * This class implements the Serializable interface so that it can be
 * a serializable field within User (which must be serialized).
 * This class has the appropriate getters and setters for its two fields:
 * tag-type and tag-value. Throughout the program, these may be shortened to 
 * tt and tv for ease of typing.
 * 
 */

import java.io.*;

public class Tag implements Serializable{
    private String tagType;
    private String tagValue;
    
    public Tag (String tagType, String tagValue){
	this.tagType = tagType;
	this.tagValue = tagValue;
    }
    
    public String getType(){
    	return tagType;
    }
    
    public void setType(String type){
    	tagType = type;
    }
    
    public String getValue(){
    	return tagValue;
    }
    
    public void setValue(String value){
    	tagValue = value;
    }
    
    public String toString(){
    	return this.tagType + ", " + this.tagValue;
    }
}