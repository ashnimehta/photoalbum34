package model;

 /**
 * @author Michelle Chen
 * @author Ashni Mehta
 * 
 * This class maps out what data a User holds
 * and what a User should be able to do in our
 * application. This class implements the Serializable
 * interface so that it can be serialized into 
 * .ser files and deserialized whenever the appropriate 
 * user logs into the application. A user has a username,
 * with which they log into the application, and an 
 * ArrayList of albums. This class also provides functionality
 * for searching by tag-value and searching by date range, as those
 * are both operations that a user of our application should be able
 * to do.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

public class User implements Serializable{
	private String username;
	private ArrayList<Album> albums;	
	
	public User(String username){
		this.username = username;
		this.albums = new ArrayList<Album>();
	}
	
	public ArrayList<Album> getAlbums(){
		return this.albums;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public void addAlbum(String title){
		this.albums.add(new Album(username, title));
	}
	
	public ArrayList<Photo> searchByTagPair(String tt, String tv){
		ArrayList<Photo> results = new ArrayList<Photo>();
		for(int i = 0; i < this.getAlbums().size(); i++){
			for(int j = 0; j < this.getAlbums().get(i).getPhotos().size(); j++){
				ArrayList<Tag> temp_tags = new ArrayList<Tag>();
				temp_tags = this.getAlbums().get(i).getPhotos().get(j).getTags();
				for(int k = 0; k<temp_tags.size(); k++){
					if(temp_tags.get(k).getValue().equals(tv) && temp_tags.get(k).getType().equals(tt)){
						results.add(this.getAlbums().get(i).getPhotos().get(j));
					}
				}
			}
		}
		return results;
	}
	
	public ArrayList<Photo> searchByTags(String tv){
		ArrayList<Photo> results = new ArrayList<Photo>();
		for(int i = 0; i < this.getAlbums().size(); i++){
			for(int j = 0; j < this.getAlbums().get(i).getPhotos().size(); j++){
				ArrayList<Tag> temp_tags = new ArrayList<Tag>();
				temp_tags = this.getAlbums().get(i).getPhotos().get(j).getTags();
				for(int k = 0; k<temp_tags.size(); k++){
					if(temp_tags.get(k).getValue().equals(tv)){
						results.add(this.getAlbums().get(i).getPhotos().get(j));
					}
				}
			}
		}
		return results;
	}
	
	public ArrayList<Photo> searchByDate(Calendar calStart, Calendar calEnd){
		ArrayList<Photo> results = new ArrayList<Photo>();
		for(int i = 0; i < this.getAlbums().size(); i++){
			for(int j = 0; j < this.getAlbums().get(i).getPhotos().size(); j++){
				if(this.getAlbums().get(i).getPhotos().get(j).getDateCal().before(calStart)){
					continue;
				}
				else if(this.getAlbums().get(i).getPhotos().get(j).getDateCal().after(calEnd)){
					continue;
				}
				else{
					//add to array list, it is neither before range nor after range
					results.add(this.getAlbums().get(i).getPhotos().get(j));
				}
			}
		}
		return results;
	}
}